# -*- coding: utf-8 -*-
emissions_path = 'inceneritore.csv'
stations_path = 'centraline/stazioni.csv'
albareto_path = 'centraline/albareto'
drive_path = 'centraline/drive'

from collections import defaultdict
from datetime import datetime, timedelta
import glob
import numpy as np
from matplotlib import pylab
from scipy import stats
import scipy
import logging
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

try:
    from IPython.core.display import display, HTML
    hprint = lambda *e: display(HTML('<br>'.join(map(str,e))))
except:
    hprint = print

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


class iperparam():
    """
    Model iper-parameters, defining important aspects of the calculation.
    """
    ########################
    # analyze_stops defaults
    margin_fit = 1 # Degree of curve fitting for stop interpolation
    pre_margin = 20 # In relative analysis, consider this number of days before each stop
    post_margin = 6 # Consider this number of days after each stop
    shift = 4 # Skip first days from stop, before starting analysis
    relative = 0 # Calculate variation after the stop as relative to the variation before the stop
    pre_mean = 0
    limit = 2 # Discard stops shorter than this number of days
    limit_max = 100
    weight = 1
    
    
    #########################
    # temperature_norm climate model defaults 

    Td = 3 # Temperature fit degree 3
    Ld = 3 # Radiation fit degree 1
    Dd = 3 # Time drift fit degree 1
    Wd = 1 # Wind fit degree 1
    Rd = 1 # Rain fit degree 1
    Sd = 3 # Cyclic weekday fit degree 3
    Bd = 3 # Benzene fit degree
    
    sm = 7 # Smoothing factor
    sk = 1 # Skewed smoothing direction


S_SO2, S_PM10, S_O3, S_NO2, S_NOX, S_CO, S_C6H6, S_C6H5CH,\
    S_NO, S_C6H4, S_PM25  = 1,5,7,8,9,10,20,21,38,82,111

params = {1: 'SO2',
    5:"PM10",
    7:"O3",
    8:"NO2",
    9:"NOX",
    10:"CO",
    20:"C6H6",
    21:"C6H5-CH",
    38:"NO",
    82:"C6H4",
    111:"PM2.5"}


# Emissions header
E_HCL, E_CO, E_SO2, E_NOX, E_HF, E_PM10, E_COT, E_NH3 = 1,2,3,4,5,6,7,8
eparams={1:"HCL",2:"CO",3:"SO2",4:"NOX",5:"HF",6:"PM10",7:"COT",8:"NH3"}

def smooth(x, window=10, method='hanning', skew=False):
    """method='flat', 'hanning', 'hamming', 'bartlett', 'blackman', 'kaiser'"""
    if not window:
        return x
    if skew:
        window *= 2
    s = np.r_[2 * x[0] - x[window - 1::-1], x, 2 * x[-1] - x[-1:-window:-1]]
    if method == 'flat':  # moving average
        w = np.ones(window, 'd')
    else:
        w = eval('np.' + method + '(window)')
    if skew>0:
        w[:int(window//2)] = 0
    elif skew<0:
        w[int(window//2):] = 0
    y = np.convolve(w / w.sum(), s, mode='same')
    y = y[window:-window + 1]
    return y

def calc_spans(boxes):
    spans = []
    for i,ln in enumerate(boxes[:-1]):
        M = boxes[i+1]
        if ln==M-1:
            spans.append(str(M))
        else:
            spans.append('{}-{}'.format(ln,M))
    return np.array(spans)

def group_by_shutdown_length(lengths, deltas, dates, boxes=False, step=1, select_month=False, empty=False):
    impacts = []
    freqs = []
    spans = []
    if not boxes:
        boxes = False
        for ln in range(1,lengths.max()+2,step):
            boxes.append(ln, ln+step)
    if select_month:
        months = np.array([d.month for d in dates])
        mask = months==select_month
        if mask.sum()==0:
            return spans, impacts, freqs
        lengths = lengths[mask]
        deltas = deltas[mask]
        dates = dates[mask]
    for i, ln in enumerate(boxes[:-1]):
        M = boxes[i+1]
        sel = (lengths >= ln)*(lengths<M)
        #print('step',step,(ln,M),'month',select_month,lengths[sel], sel.sum())
        d=0.0
        f = sel.sum()
        if f>0:
            d = deltas[sel].mean()
        elif not empty:
            continue
        impacts.append(d)
        freqs.append(f)
        if ln==M-1:
            spans.append(str(M))
        else:
            spans.append('{}-{}'.format(ln,M))        

    return spans, impacts, freqs

def get_cmap(n, name='Dark2'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct 
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return pylab.cm.get_cmap(name, n)


def correlation_matrix(fields, corr_fields, corr_func=stats.pearsonr):
    """Build a correlation matrix according to a correlation function `corr_func`."""
    N = len(corr_fields)
    r_mat = np.zeros((N,N))
    p_mat = np.zeros((N,N))
    for i, ifield in enumerate(corr_fields):
        for j, jfield in enumerate(corr_fields):
            x, y = np.array(fields[ifield]), np.array(fields[jfield])
            r, p = corr_func(x,y)
            r = abs(r)
            r_mat[j,i] = r
            p_mat[j,i] = p
    return r_mat, p_mat

string_keys = [0,1,2,3,4,7,8]

# Cod_Staz replaced by sid key
def load_stations(path=stations_path):
    stations = {}
    sensors = defaultdict(list)
    keys = []
    for line in open(path,'r'):
        line=line.replace('\n','').split(',')
        if not keys:
            keys=line
            keys[1] = 'sid'
            continue
        if line[2]not in ('MODENA',): # 'SASSUOLO','FIORANO MODENESE', 'MIRANDOLA'):
            continue
        data = {}
        for i,v in enumerate(line):
            if i not in string_keys:
                if not v:
                    v = 0
                elif '.' in v:
                    v = float(v)
                else:
                    v = int(v)
            data[keys[i]] = v
        data['sid'] = data['sid'].replace('.','')
        stations[data['sid']] = data
        sensors[data['sid']].append(data['Id_Param'])
    
    return stations, sensors

def load_emissions(path=emissions_path):
    data = []
    lastd = None
    for line in open(path,'r'):
        line = line.replace('\n','').split(';')
        if len(line)!=9:
            logging.debug('Skip {}'.format(line))
            continue
        if line[0].count('/')!=2:
            logging.debug('Skip {}'.format(line))
            continue
        dt = datetime.strptime(line[0], '%d/%m/%Y')
        days =[]
        if lastd is None:
            days.append(dt)
        elif (dt-lastd).days>0:
            for i in range((dt-lastd).days):
                d = lastd+timedelta(days=i+1)
                days.append(d)
        lastd = dt
        try:
            v = list(map(float, line[1:]))
        except:
            # Shut down: zero emissions
            v = [0.0]*8
        for d in days:
            data.append([d]+v)
    return np.array(data)


def uniform_days(data):
    t = data[:,0]
    dd = (t[-1]-t[0]).days
    
    dt = t-t[0]
    # Hourly dt
    hdt = np.array([dt.days*24+dt.seconds/3600. for dt in (dt)])
    # New hourly dt
    newt = np.arange(dd, dtype=float)*24
    # Same-day index
    dindex = np.array([dt.days for dt in (dt)])
    # Output day array
    t0 = datetime(*t[0].timetuple()[:3])
    newd = np.array([t0+timedelta(days=i) for i in range(dd)])
    vvals=[newd]
    ###########
    # Sub-day aggregation
    for delta in range(dd):        
        # Search for sub-day indexes
        sel = np.where(dindex==delta)[0]
        if not len(sel)>1:
            continue
        for vi in range(data.shape[1]-1):
            # Aggregate sub-day indexes by setting them equal their mean
            v = data[sel,vi+1]
            data[sel,vi+1] = np.mean(v)
    #print('AAAAA',(data[:,1]<0).sum())
    ###########
    # Daily extrapolation
    # Remove equal dates
    mask = np.concatenate(([True],np.diff(hdt)!=0))
    hdt = hdt[mask]
    data = data[mask]
    for vi in range(data.shape[1]-1):
        v = data[:,vi+1].astype(float)
        # Hourly interpolation
        f = scipy.interpolate.UnivariateSpline(hdt,v,k=1,s=0)
        # Daily extrapolation
        v1 = f(newt)
        vvals.append(v1)
    data = np.vstack(vvals).T
    return data
    


def load_station_cron(path, sensors):
    global mindate,maxdate
    data = []
    sid = 0
    param = 0
    keys = []
    for i,line in enumerate(open(path,'r')):
        line=line.replace('\n','').split(',')
        if not keys:
            keys = line
            continue
        if 'DATA_INIZIO' in keys:
            val = float(line[-2])            
            df = datetime.strptime(line[3], '%d/%m/%Y %H')
            p = int(line[1])
        # Temperatures, rain,...
        elif 'Fine validità (UTC)' in keys:
            try:
                val = float(line[-1])
            except:
                print(line)
                raise
            df = datetime.strptime(line[1].split(' ')[0], '%Y-%m-%d')
            p = -1
        else:
            val = float(line[-1])
            df = datetime.strptime(line[2], '%d/%m/%Y %H:%M')
            p = int(line[1])
            
        if not param:
            param = p
        
        if param>0:
            s = line[0]
            if not sid:
                sid = s
            assert s==sid, '{}!={}'.format(s,sid)
            assert p==param
            assert p in sensors
        
        data.append([df, val])
    
    return data

def load_full_station_cron(station, sensors, zeroday, endday):
    data = {}
    for sensor in sensors:
        s = station
        if len(s)<8:
            s='0'+s
        path = '{}/{}_{:03d}_*.csv'.format(drive_path,s,sensor)
        path1 = '{}/storico_*_{}_{:03d}.csv'.format(drive_path,s,sensor)
        sdata = []
        logging.debug(path1)
        logging.debug('{}'.format(glob.glob(path)+glob.glob(path1)))
        for spath in glob.glob(path)+glob.glob(path1):
            logging.debug('Scanning '+spath)
            part = load_station_cron(spath, sensors)
            logging.debug('Loaded {}'.format(len(part)))
            sdata+=part
        #print('For station {} sensor {}, loaded {}'.format(station,sensor,len(sdata)))
        # Order by day from zeroday
        sdata = sorted(sdata, key=lambda e: e[0])
        sdata = np.array(sdata)
        mask = (sdata[:,0]>zeroday)*(sdata[:,0]<endday)
        #print(sdata.shape,sdata[:10])
        sdata = uniform_days(sdata[mask])
        #print(sdata.shape,sdata[:10])
        data[sensor] = sdata
        
        logging.debug('Loaded total sensor {} for station {}: {}, deleted {}'.format(station,
                                        sensor,sdata.shape, np.sum(mask==0)))
    return data


def load_albareto_sensor(path):
    data = np.loadtxt(path, delimiter='#', comments='&&&&',dtype=str, skiprows=1)
    values = data[:,2].astype(float)
    dates = data[:,1]
    days = np.array([d[:11] for d in dates])
    ndays = []
    means = []
    buf = []
    for d in days:
        if ndays and d==ndays[-1]:
            continue
        i = np.where(days==d)[0]
        means.append(values[i].mean())
        ndays.append(d)
    ndays = [datetime(*[int(e) for e in d.split('/')][::-1]) for d in ndays]
    #pylab.plot(ndays,means)
    #pylab.show()
    return np.array(ndays), np.array(means)

#load_albareto_sensor(albareto_path+'/qaria_04000023_038.csv')

def load_full_albareto_cron(zeroday, endday):
    data = {}
    keys = [5,8,9,38]
    for sensor in keys:
        ndays,means = load_albareto_sensor(albareto_path+'/qaria_04000023_{:03d}.csv'.format(sensor))
        mask = (ndays>zeroday)*(ndays<endday)
        sdata = np.array([ndays[mask],means[mask]]).transpose()
        sdata = uniform_days(sdata)
        data[sensor] = sdata
        #print(sdata.shape)
        #pylab.plot(sdata[:,0], sdata[:,1])
        #pylab.show()
    return data, keys

#load_full_albareto_cron(datetime(2010,1,1),datetime(2020,1,1))

def fix_correlated_map(cmap,zeroday,endday):
    mindate,maxdate=zeroday,endday
    logging.debug('Initial range {} --> {}'.format(mindate,maxdate))
    for k,dat in cmap.items():
        if dat[0][0]>mindate:
            mindate = dat[0][0]
        elif dat[-1][0]<maxdate:
            maxdate = dat[-1][0]
    logging.debug('Selecting between {} --> {}'.format(mindate,maxdate))
    new = {}
    mindate = datetime(*mindate.timetuple()[:3])
    maxdate = datetime(*maxdate.timetuple()[:3])
    for k,dat in cmap.items():
        cron = dat[:,0]
        mask = (cron<=maxdate)*(cron>=mindate)
        new[k]=dat[mask,1].astype(float)
        logging.debug(str((k,len(cron), len(new[k]), dat[mask][0,0], dat[mask][-1,0])))
    new['day'] = np.arange(len(new[k]))
    new['date'] = np.array([mindate+timedelta(days=int(i)) for i in new['day']])
    wd0 = new['date'][0].weekday()
    new['weekday'] = (new['day']+wd0)%7
    return new, mindate,maxdate


def weekday_func(var,w):
    m = var.mean()
    md = []
    for d in range(7):
        md.append(var[w==d].mean())
    weights = np.array(md)-m
    return weights[w], weights

def temperature_norm_0(corr_map, key):
    sm, sk = iperparam.sm, iperparam.sk
    
    res = []
    T = corr_map['T']
    #T = smooth(corr_map['T'],sm,skew=sk)
    L = corr_map['L']
    #W = smooth(corr_map['W'],sm,skew=sk)
    W = corr_map['W']
    R = smooth(corr_map['R'],sm,skew=sk)
    
    Bd = iperparam.Bd
    if key=='C6H602':
        Bd = 0
        
    model = corr_map[key].copy()
    
    
    # Temperature fit
    if iperparam.Td:
        full = np.polyfit(T,model, iperparam.Td, full=True)
        fit = np.polyval(full[0],T)
        res += list(full[0])
    else:
        fit = np.zeros(T.shape)
    model -= fit
    
    
    # Time fit
    if iperparam.Dd:
        full = np.polyfit(corr_map['day'],model, iperparam.Dd, full=True)
        tfit = np.polyval(full[0],corr_map['day'])
        res += list(full[0])
    else:
        tfit = np.zeros(T.shape)
    
    model -= tfit
    
    # Benzene fit
    if Bd:
        B = corr_map['C6H602']
        full = np.polyfit(B, model, Bd, full=True)
        Bfit = np.polyval(full[0],B)
        res += list(full[0])
    else:
        Bfit = np.zeros(T.shape)
    model -= Bfit
    
    
    # Sunday fit
    if iperparam.Sd:
        weekday, weights = weekday_func(corr_map[key], corr_map['weekday'])
        full = np.polyfit(weekday, model, iperparam.Sd, full=True)
        sfit = np.polyval(full[0],weekday)
        res += list(full[0])
    else:
        sfit = np.zeros(T.shape)
        weights = np.zeros(7)
    
    model -= sfit
    
    # Radiation fit
    if iperparam.Ld:
        full = np.polyfit(L,model, iperparam.Ld, full=True)
        Lfit = np.polyval(full[0],L)
        res += list(full[0])
    else:
        Lfit = np.zeros(T.shape)
    
    model -= Lfit
    
    # Wind fit
    if iperparam.Wd:
        full = np.polyfit(W, model, iperparam.Wd, full=True)
        wfit = np.polyval(full[0], W) 
        res += list(full[0])
    else:
        wfit = np.zeros(T.shape)
        
    model -= wfit
        
    # Rain fit
    if iperparam.Rd:
        full = np.polyfit(R, model, iperparam.Rd, full=True)
        rfit = np.polyval(full[0], R)
        res += list(full[0])
    else:
        rfit = np.zeros(T.shape)
        
    model -= rfit
    
    corr_map['m'+key]=fit+Bfit+tfit+sfit+wfit+rfit+Lfit
    corr_map['_'+key]=model
    corr_map['f'+key]=fit
    corr_map['L'+key]=Lfit
    corr_map['t'+key]=tfit
    corr_map['s'+key]=sfit
    corr_map['b'+key]=Bfit
    corr_map['weights_'+key]=weights
    corr_map['ww_'+key] = weights[corr_map['weekday']]
    corr_map['w'+key]=wfit
    corr_map['r'+key]=rfit
    corr_map['params_'+key] = res
    return res
    
    


def _temperature_model(x, corr_map, key):
    Td, Dd, Sd, Ld, Wd, Rd, Bd = iperparam.Td, iperparam.Dd, iperparam.Sd,\
                            iperparam.Ld, iperparam.Wd, iperparam.Rd, iperparam.Bd
    sm, sk = iperparam.sm, iperparam.sk
    T = corr_map['T']
    B = corr_map['C6H602']
    if key=='C6H602':
        B = corr_map['NOX02']
    #T = smooth(corr_map['T'],sm,skew=sk)
    L = corr_map['L']
    W = smooth(corr_map['W'],sm,skew=sk)
    R = smooth(corr_map['R'],sm,skew=sk)
    
    s,e=0,0
    
    if Td:
        e = Td+1
        yT = np.polyval(x[s:e], T)
        s = e
    else:
        yT = np.zeros(len(T))
    
    if Dd:
        e = s+Dd+1
        yD = np.polyval(x[s:e],corr_map['day'])
        s = e
    else:
        yD = np.zeros(len(yT))
        
    if Bd:
        e = s+Dd+1
        yB = np.polyval(x[s:e], B)
        s = e
    else:
        yB = np.zeros(len(yT))
    
    if Sd:
        e = s+Sd+1
        weekday, weights = weekday_func(corr_map[key], corr_map['weekday'])
        yS = np.polyval(x[s:e],weekday)
        s = e
    else:
        yS = np.zeros(len(yT))
        weights = np.zeros(7)
    
    if Ld:
        e = s+Ld+1
        yL = np.polyval(x[s:e],L)
        s = e
    else:
        yL = np.zeros(len(yT))
    
    if Wd:
        e = s+Wd+1
        yW = np.polyval(x[s:e], W)
        s = e
    else:
        yW = np.zeros(len(yT))
        
    if Rd:
        e = s+Rd+1
        yR = np.polyval(x[s:e], R)
        s = e
    else:
        yR = np.zeros(len(yT))
        
    return yT, yB, yL, yD, yW, yR, yS, weights
    
def temperature_model(x, corr_map, key):
    yT, yB, yL, yD, yW, yR, yS, ww = _temperature_model(x, corr_map, key)
    model = yT+yB+yL+yD+yW+yR+yS
    return np.sqrt(((corr_map[key]-model)**2).sum())
    
def temperature_norm_1(corr_map, key):
    tot = np.array([iperparam.Td, iperparam.Dd, iperparam.Sd,\
                    iperparam.Ld, iperparam.Wd, iperparam.Rd, iperparam.Bd])
    tot = tot.sum()+tot.astype(bool).sum()
    res = temperature_norm_0(corr_map, key)
    res = scipy.optimize.minimize(temperature_model, res, 
                                  method='Nelder-Mead',
                                  args=(corr_map, key))
    
    yT, yB, yL, yD, yW, yR, yS, ww = _temperature_model(res.x, corr_map, key)
    model = (yT+yB+yL+yD+yW+yR+yS)
    
    corr_map['m'+key]=model
    corr_map['_'+key]=corr_map[key]-model
    corr_map['f'+key]=yT
    corr_map['b'+key]=yB
    corr_map['L'+key]=yL
    corr_map['t'+key]=yD
    corr_map['w'+key]=yW
    corr_map['r'+key]=yR
    corr_map['s'+key]=yS
    corr_map['weights_'+key] = ww
    corr_map['params_'+key] = res.x

temperature_norm = temperature_norm_0

def build_corr_map(corr_map, stations, station_data, emissions, zeroday, endday):
    for s in stations.keys():
        for p in [S_PM10,S_NO2,S_NO, S_O3, S_C6H6]: #5,8,38
            dat = station_data[s]
            if p in dat:
                dat = dat[p]
            else:
                continue
            corr_map[params[p]+s[-2:]] = dat
    for ei,ek in eparams.items():
        corr_map['E_'+ek] = emissions[:,[0,ei]]


    corr_map,mindate,maxdate = fix_correlated_map(corr_map,zeroday,endday)
    corr_map['NO2mm']=(corr_map['NO202']+corr_map['NO222']+corr_map['NO223'])/3
    corr_map['NOmm']=(corr_map['NO02']+corr_map['NO22']+corr_map['NO23'])/3
    corr_map['NOX02']=(corr_map['NO202']+corr_map['NO02'])/2
    corr_map['NOX22']=(corr_map['NO222']+corr_map['NO22'])/2
    corr_map['NOX23']=(corr_map['NO223']+corr_map['NO23'])/2
    corr_map['NOXmm']=(corr_map['NOX02']+corr_map['NOX22']+corr_map['NOX23'])/3

    temperature_norm(corr_map, 'NO202')
    temperature_norm(corr_map, 'NO222')
    temperature_norm(corr_map, 'NO223')
    temperature_norm(corr_map, 'NO2mm')
    temperature_norm(corr_map, 'NO02')
    temperature_norm(corr_map, 'NO22')
    temperature_norm(corr_map, 'NO23')
    temperature_norm(corr_map, 'NOmm')
    temperature_norm(corr_map, 'NOX02')
    temperature_norm(corr_map, 'NOX22')
    temperature_norm(corr_map, 'NOX23')
    temperature_norm(corr_map, 'NOXmm')

    corr_map['PM10mm']=(corr_map['PM1002']+corr_map['PM1022']+corr_map['PM1023'])/3
    temperature_norm(corr_map, 'PM1022')
    temperature_norm(corr_map, 'PM1002')
    temperature_norm(corr_map, 'PM1023')
    temperature_norm(corr_map, 'PM10mm')

    temperature_norm(corr_map, 'C6H602')
    temperature_norm(corr_map, 'O322')
    return corr_map,mindate,maxdate
    
def _model_params():
    Td, Dd, Sd, Ld, Wd, Rd, Bd = iperparam.Td, iperparam.Dd, iperparam.Sd,\
                            iperparam.Ld, iperparam.Wd, iperparam.Rd, iperparam.Bd
    
    s,e=0,0
    model_params = {}
    if Td:
        e = e+Td+1
        model_params['f'] = ('T',s,e)
        s = e
    else:
        model_params['f'] = ('T',0,0)

    if Dd:
        e = e+Dd+1
        model_params['t'] = ('day',s,e)
        s = e
    else:
        model_params['t'] = ('day',0,0)
        
    if Bd:
        e = e+Bd+1
        model_params['b'] = ('C6H602',s,e)
        s = e
    else:
        model_params['b'] = ('C6H602',0,0)

    if Sd:
        e = e+Sd+1
        model_params['s'] = ('weekday',s,e)
        s = e
    else:
        model_params['s'] = ('weekday',0,0)

    if Ld:
        e = e+Ld+1
        model_params['L'] = ('L',s,e)
        s = e
    else:
        model_params['L'] = ('L',0,0)

    if Wd:
        e = e+Wd+1
        model_params['w'] = ('W',s,e)
        s = e
    else:
        model_params['w'] = ('W',0,0)

    if Rd:
        e = e+Rd+1
        model_params['r'] = ('R',s,e)
        s = e
    else:
        model_params['r'] = ('R',0,0)
    return model_params



def get_param_curve(key, corr_map, n=1000):
    """Rebuild model components"""
    model_params = _model_params()
    var,s,e=model_params[key[0]]
    x = corr_map[var]
    if var=='weekday':
        x = np.arange(7)
    elif var!='day':
        x = np.linspace(x.min(), x.max(),n)
    if s==e:
        return x, np.zeros(len(x))
    
    par = corr_map['params_'+key[1:]][s:e]
    if var=='weekday':
        x1 = corr_map['weights_'+key[1:]]
        y = np.polyval(par, x1)
    else:
        y = np.polyval(par, x)
    if var=='day':
        x = corr_map['date']
    return x,y
    
def model_panel(var, pollutant, corr_map):
    nrows, ncols = 2,4
    fig = pylab.figure()
    pylab.title('Componenti del modello climatico')
    ax = pylab.subplot(nrows, ncols,1)
    x,y = get_param_curve('f'+var, corr_map)
    ax.plot(x,y, label='Temperatura (°C)')
    ax.plot(corr_map['T'],corr_map['_'+var]+corr_map['f'+var], '.',alpha=0.2)
    d = 0.1*(y.max()-y.min())
    ax.set_ylim(y.min()-d, d+y.max())
    ax.set_xlabel('Temperatura (°C)')
    #ax.legend()
    ax.set_ylabel(pollutant+' ($\mu g/m^3$)')

    ax = pylab.subplot(nrows, ncols,5)
    x,y = get_param_curve('L'+var, corr_map)
    ax.plot(x,y, label='Rad. ($J/m^2$)')
    ax.plot(corr_map['L'],corr_map['_'+var]+corr_map['L'+var], '.',alpha=0.2)
    d = 0.1*(y.max()-y.min())
    ax.set_ylim(y.min()-d, d+y.max())
    ax.set_xlabel('Radiazione ($J/m^2$)')
    #ax.legend()

    ax = pylab.subplot(nrows, ncols,2)
    x,y = get_param_curve('t'+var, corr_map)
    ax.plot(x,y, label='Data')
    ax.plot(corr_map['date'],corr_map['_'+var]+corr_map['t'+var],'.', alpha=0.2)
    d = 0.1*(y.max()-y.min())
    ax.set_ylim(y.min()-d, d+y.max())
    ax.set_xticks(list(x[::700])+[x[-1]])
    ax.set_xlabel('Deriva temporale')
    #ax.legend()

    ax = pylab.subplot(nrows, ncols,4)
    x,y = get_param_curve('s'+var, corr_map)
    ax.plot(x,y, 'o-', label='Settimanale')
    ax.plot(corr_map['weekday'],corr_map['_'+var]+corr_map['s'+var],'.', alpha=0.2)
    d = 0.1*(y.max()-y.min())
    ax.set_ylim(y.min()-d, d+y.max())
    ax.set_xticks(x)
    ax.set_xticklabels(['L','Ma','Me','G','V','S','D'])
    ax.set_xlabel('Giorno della settimana')
    #ax.legend()
    ax.set_ylabel(pollutant+' ($\mu g/m^3$)')

    ax = pylab.subplot(nrows, ncols,6)
    x,y = get_param_curve('r'+var, corr_map)
    ax.plot(x,y, label='Pioggia (mm)')
    ax.plot(corr_map['R'],corr_map['_'+var]+corr_map['r'+var],'.', alpha=0.2)
    d = 0.5*(y.max()-y.min())
    ax.set_ylim(y.min()-d, d+y.max())
    ax.set_xlabel('Precipitazioni (mm)')
    #ax.legend()

    ax = pylab.subplot(nrows, ncols,7)
    x,y = get_param_curve('w'+var, corr_map)
    ax.plot(x,y, label='Vento (m/s)')
    ax.plot(corr_map['W'],corr_map['_'+var]+corr_map['w'+var],'.', alpha=0.2)
    d = 0.5*(y.max()-y.min())
    ax.set_ylim(y.min()-d, d+y.max())
    #ax.ticklabel_format(scilimits=(1,-1))
    ax.set_xlabel('Vento (m/s)')
    #f=ax.legend()
    
    ax = pylab.subplot(nrows, ncols,3)
    x,y = get_param_curve('b'+var, corr_map)
    ax.plot(x,y, label='Benzene ($\mu g/m^3$)')
    ax.plot(corr_map['C6H602'],corr_map['_'+var]+corr_map['b'+var],'.', alpha=0.2)
    d = 0.1*(y.max()-y.min())
    ax.set_ylim(y.min()-d, d+y.max())
    ax.set_xlabel('Benzene ($\mu g/m^3$)')

    fig.subplots_adjust(hspace=.5)
    


def reject_outliers(data, m=2, std=False):
    if not std:
        std = np.std(data)
    mask = abs(data - np.mean(data)) < m * std
    return mask

def identify_zero_emission_days(emissions):
    totals = emissions[:,1:].sum(1)
    days = np.where(totals==0)[0]
    diff = np.diff(days)
    segments = np.where(diff>1)[0]
    events = [] # start,end
    start = days[0]
    for sg in segments:
        # End of previous
        end = days[sg]
        events.append((start, end))
        assert totals[start:end+1].sum()==0
        # Start new one
        start = days[sg+1]
    return events


week_day_names = ['Lunedì','Martedì','Mercoledì','Giovedì','Venerdì', 'Sabato','Domenica']
days = np.arange(7)

def zero_emission_by_weekday(stops, emdates):
    daylength = np.array([(emdates[i].weekday(), e-i+1) for i,e in stops])
    counts = [(daylength[:,0]==d).sum() for d in days]
    lengths = [daylength[(daylength[:,0]==d)].mean() for d in days]
    fig = pylab.figure()
    ax = pylab.subplot(1,1,1)
    ax.set_xticklabels(['']+week_day_names)
    ax.set_ylabel('Numero di fermate')
    ax.bar(days, counts, color='orange', label='Numero')
    ax2 = ax.twinx()
    ax2.bar(days, lengths, width=0.5, label='Durata')
    ax2.set_ylabel('Durata media (gg)')
    ax.legend(loc=2)
    ax2.legend(loc=1)
    pylab.title('Numero di fermate per giorno di inizio e durata media')
    


def weekday_dependency(corr_map, pollutant, prefix=''):
    means = []
    means2 = []
    means3 = []
    means4 = []
    for d in days:
        mask = corr_map['weekday']==d
        means.append(corr_map[prefix+pollutant+'02'][mask].mean())
        means2.append(corr_map[prefix+pollutant+'22'][mask].mean())
        means3.append(corr_map[prefix+pollutant+'mm'][mask].mean())
        means4.append(corr_map[prefix+pollutant+'23'][mask].mean())

    means, means2,means4 = np.array(means), np.array(means2),np.array(means4)
    g_mean = (corr_map[pollutant+'02'][reject_outliers(corr_map[pollutant+'02'])].mean())
    f_mean = (corr_map[pollutant+'22'][reject_outliers(corr_map[pollutant+'22'])].mean())
    m_mean = (corr_map[pollutant+'mm'][reject_outliers(corr_map[pollutant+'mm'])].mean())
    a_mean = (corr_map[pollutant+'23'][reject_outliers(corr_map[pollutant+'23'])].mean())
    q = 100 if prefix else 0
    pmeans = q+100*(means-g_mean)/g_mean
    pmeans2 = q+100*(means2-f_mean)/f_mean
    pmeans3 = q+100*(means3-m_mean)/m_mean
    pmeans4 = q+100*(means4-a_mean)/a_mean

    pylab.figure()
    ax= pylab.subplot(2,1,1)
    ax.plot(days, means, '-o', label='Giardini')
    ax.plot(days, means3,'-o', label='Media')
    ax.plot(days, means2,'-o', label='Ferrari')
    ax.plot(days, means4,'-o', label='Albareto')

    ax.legend()
    ax.set_ylabel('Conc. ($\mu g/m^3$)')
    f = ax.set_xticklabels(['']*8)
    ax2= pylab.subplot(2,1,2)
    ax2.bar(days, pmeans, label='Giardini')
    ax2.bar(days, pmeans3, width=0.6, label='Media')
    ax2.bar(days, pmeans2, width=0.4, label='Ferrari')
    ax2.bar(days, pmeans4, width=0.2, label='Albareto')
    ax2.set_ylabel('Var. (%)')
    #ax2.legend()
    ax.set_title('Concentrazione di {} per giorno della settimana'.format(pollutant))

    f = ax2.set_xticklabels(['']+week_day_names)
    
def cdelta_linear(v,iw): 
    n = len(v)
    x = np.arange(n)
    w = np.ones(n)
    w[iw]=iperparam.weight
    fv = np.polyfit(x, v, 1, w=w)
    vm = fv[0]*(n-1)
    return fv, vm

def cdelta(v, deg, iw=0):
    if deg==1:
        return cdelta_linear(v,iw)
    n = len(v)
    # Calculate delta from fitted line
    x = np.arange(n)
    w = np.ones(n)
    w[iw]=iperparam.weight
    fv = np.polyfit(x, v, deg, w=w)
    
    dv = np.polyder(np.poly1d(fv))
    roots = dv.r
    eps = np.finfo(float).eps
    roots = np.real_if_close(roots, 1)
    roots = [np.real(e) for e in roots]
    roots = list(filter(lambda e: (e>0 and e<n), roots))
    yroots = np.polyval(fv,roots)
    reals = np.real_if_close(yroots, 1)
    if not len(reals):
        #print('NO ROOT, reverting to linear fitting')
        return cdelta_linear(v,iw)
    reals = [np.real(e) for e in reals]
    #print(roots, yroots)
    vm = min(reals)-v[0]
    #vm = fv[0]*(n-1)  
    return fv, vm

def analyze_stops(stops, emissions, mindate, corr_map, 
                  pre_margin=None, 
                  post_margin=None, var='NO222', 
                  limit=None, limit_max=None, 
                  shift=None,
                 relative=None,
                 pre_mean=None,
                 margin_fit=None):
    
    if post_margin is None:
        post_margin = iperparam.post_margin
    if pre_margin is None:
        pre_margin = iperparam.pre_margin or post_margin
    if limit is None:
        limit = iperparam.limit
    if limit_max is None:
        limit_max = iperparam.limit_max
    if shift is None:
        shift = iperparam.shift
    if relative is None:
        relative = iperparam.relative
    if pre_mean is None:
        pre_mean = iperparam.pre_mean
    if margin_fit == None:
        margin_fit = iperparam.margin_fit
        
    N = len(corr_map['day'])
    ratios = []
    outvals = []
    dates = []
    deltas = []
    lengths = []
    fitvals = []
    #svar = smooth(corr_map[var])
    svar = corr_map[var]
    var1=var
    if var1.startswith('_'):
        var1 = var[1:]
    model = corr_map['m'+var1].copy()
    for start,end in stops:
        d = emissions[start][0]
        si = (d-mindate).days
        if si<pre_margin:
            logging.debug('Skip stop {} {}'.format(emissions[start][0], mindate))
            continue
        ei = (emissions[end][0]-mindate).days
        if si>=N-1-post_margin:
            continue
        if ei-si<limit-1:
            #print('Limited:',d,ei-si)
            continue
        if ei-si>limit_max:
            #print('Limited:',d,ei-si)
            continue
        dates.append(d)
        
        spre = slice(si-pre_margin,si)
        spost = slice(si+shift,si+1+shift+post_margin)
        #spost = slice(ei+shift,ei+1+post_margin)
        vpre,vpost = svar[spre].copy(), svar[spost].copy()
        if pre_mean:
            vpost[0]=vpre.mean()
            vpre[-1]=vpre.mean()
        if vpre.shape[0]<3 or vpost.shape[0]<3:
            continue
        mpre = model[spre]
        mpost = model[spost]
        

        
        fv0, vm0 = cdelta(vpre, margin_fit, iw=-1)
        vm0 = fv0[0]*(len(vpost)-1)
        fv1, vm1 = cdelta(vpost, margin_fit, iw=0)
        
        if not relative:
            vm0 = 0
        delta = vm1-vm0
        
        
        slc = slice(si-pre_margin,si+2+post_margin)
        div = model[slc].mean()
        
        r = delta/div
        ratios.append(r)
        deltas.append(delta)
        vals = np.concatenate((vpre, vpost))
        outvals.append((vpre,vpost))
        lengths.append(ei-si)
        fitvals.append((fv0,fv1))
    dates, ratios, deltas = np.array(dates), np.array(ratios), np.array(deltas)
    return dates, (outvals, fitvals), ratios, deltas, np.array(lengths)


def plot_all_stops(dates, outvals, deltas, shift=None, fit=False, group=9, pre=False):
    if shift is None:
        shift = iperparam.shift
    cmap = get_cmap(group)
    cj = -1
    for i,(vpre,vals) in enumerate(outvals[0]):
        fitvals = outvals[1][i]
        j = i//group
        if cj!=j:
            pylab.figure(figsize=(9,3.5))
            pylab.xlabel("Giorni dallo stop")
            pylab.ylabel(r"Variazione ($\mu g/m^3$)")
            cj = j
        zero = vals[0]
        vals=vals-zero
        n = np.arange(len(vals))+shift
        c = cmap(i%group)
        lbl = dates[i].strftime('%d/%m/%y')
        lbl += ' {:+.1f}'.format(deltas[i])
        if fit:
            pylab.plot(n,vals,'o--', label=lbl, color=c,alpha=0.5)
            f = np.polyval(fitvals[1], n-shift)
            pylab.plot(n, f-zero, color=c)
            #pylab.plot(n1, f-zero, color=c)
            if pre:
                n1 = np.arange(len(vpre))
                npre = -n1[::-1]
                pylab.plot(npre,vpre-zero,'o--', label=lbl, color=c,alpha=0.5)
                f = np.polyval(fitvals[0], n1)
                pylab.plot(npre, f-zero, color=c)
        else:
            pylab.plot(n,vals, label=lbl, color=c)
            if pre:
                n1 = np.arange(len(vpre))
                npre = -n1[::-1]
                pylab.plot(npre,vpre-zero,'o--', label=lbl, color=c,alpha=0.5)
        pylab.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1), ncol=4, )
        if pre:
            pylab.axvline(0)
        pylab.grid(1, axis='y')


def barlabels(ax, rects, labels=False, xpos='center'):
    """
    Attach a text label above each bar in *rects*, displaying its height.

    *xpos* indicates which side to place the text w.r.t. the center of
    the bar. It can be one of the following {'center', 'right', 'left'}.
    """

    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0, 'right': 1, 'left': -1}
    
    heights = np.array([rect.get_height() for rect in rects])

    for i,rect in enumerate(rects):
        height = rect.get_height()
        if labels is not False:
            lbl = labels[i]
        else:
            lbl = '{}'.format(height)
        s = int(np.sign(height)) or 1
        ax.annotate(lbl,
                    xy=(rect.get_x() + rect.get_width() / 2, 0),
                    xytext=(offset[xpos]*3, 0),  # use 3 points offset
                    textcoords="offset points",  # in both directions
                    ha=ha[xpos], va={-1:'bottom',1:'top'}[s],
                   )
        
def extract_calendar_days(target, block_dates): #6=sunday
    sundays = []
    sundays_noblock = []
    for y in range(2012,2020):
        for m in (10,11,12,1,2,3):
            if y==2019 and m==10:
                break
            d = datetime(month=m, year=y, day=1)
            delta = target-d.weekday()
            if delta>0:
                d = datetime(month=m, year=y, day=delta+1)
            i = 0
            while d.month==m:
                sundays.append(d)
                if d.toordinal() not in block_dates:
                    sundays_noblock.append(d)
                i +=1
                d = sundays[-1]+timedelta(days=7*i)
    return sundays, sundays_noblock
        

def extract_emission_indexes(dates, emissions, vstops=None):
    res = []
    for d in dates:
        i = np.where(emissions[:,0]==d)[0][0]
        if (vstops is not None) and i in vstops:
            continue
        res.append((i,i))
    return res


def ozone_benzene_plot(fig, corr_map, pollutant, d=False):
    de = ' destagionalizzato' if d else ''
    d = '_' if d else ''
    ax2 = pylab.subplot(2,1,1)
    
    ax2.plot(corr_map['date'], corr_map[d+pollutant+'02'], '.',label=pollutant,alpha=0.7)
    ax2.set_ylabel(r'{}'.format(pollutant))

    ax1 = ax2.twinx()
    ax1.plot(corr_map['date'], corr_map[d+'C6H602'], '*', label='C6H6',color='red',alpha=0.3)
    ax1.set_ylabel('C6H6')
    ax1.set_title('Giardini: rilevazioni di benzene e '+pollutant+de)
    ax1.legend(loc=1)
    ax2.legend(loc=2)

    ax2 = pylab.subplot(2,1,2)
    ax2.plot(corr_map['date'], corr_map[d+pollutant+'22'], '.',label=pollutant,alpha=0.7)
    ax2.set_ylabel(r'{}'.format(pollutant))
    ax1 = ax2.twinx()
    ax1.plot(corr_map['date'], corr_map[d+'O322'], '*',label='O3',color='red',alpha=0.3)
    ax1.set_ylabel('O3')
    ax1.set_title('Ferrari: rilevazioni di ozono e '+pollutant+de)
    ax1.legend(loc=1)
    ax2.legend(loc=2)
